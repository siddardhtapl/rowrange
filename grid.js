var map;
var domain;
var blockAreaLayer, bodAreaLayer, gapAreaLayer, pc1AreaLayer, pc2AreaLayer, pc3AreaLayer, pc4AreaLayer;
var map;
var myPolygon;
var infoWindow;
var coords = [];
var markers = [];
var subMarkers = [];
var grid = [] ;
var arr=[];
var allPolygons = [];
var kmlLayer;
var data;
var newCoords;

var myapp = angular.module("myapp", []);

myapp.controller("mycontroller", function($scope,$http){
console.log("registered");
$scope.getJson = function() {
     $http.get('http://106.51.127.168:8000/sweet/')
     .then(function mySuccess(response) {
        //console.log(response)
        data = response.data;
        console.log(data);

    }, function myError(response) {
	console.log(response)
       data = response.statusText;
        console.log($scope.x)

    });
return data;

}


$scope.putJson=function(d){
     $http.post('http://106.51.127.168:8000/sweet/writer/', {
    data:d
  }).then(function mySuccess(response) {
  console.log(response)
        data = response.data;
        console.log(data);
        return true;
    }, function myError(response) {
	console.log(response)
        var res = response.statusText;
        console.log(res)
        return false;
    });

}

$scope.deleteJson=function(d){
     $http.post('http://106.51.127.168:8000/sweet/delete/', {
    data:d
  }).then(function mySuccess(response) {
  console.log(response)
        data = response.data;
        console.log(data)
        return true;
    }, function myError(response) {
	console.log(response)
        var res = response.statusText;
        console.log(res)
        return false;
    });
}
    });
/************************** Header Methods start **********************************************/
var updateUserName = function()
{
	var header = document.getElementById("userName");
	header.innerHTML = "Welcome, " + window.location.hash.substr(1);
}
/************************** Header Methods End **********************************************/

function myMap() {
	//mapBounds = new google.maps.LatLngBounds(new google.maps.LatLng(42.930327, -90.308659), new google.maps.LatLng(42.928990, -90.306981));
    var mapOptions = {
        center: new google.maps.LatLng(44.4823074231, -93.0325764397),
        zoom: 18,
        mapTypeId: google.maps.MapTypeId.ROADMAP,
        mapTypeControl: true,
        overviewMapControl: false,
        scaleControl: true,
        streetViewControl: false,
        panControl: true,
        zoomControl: true,
        zoomControlOptions: { style: google.maps.ZoomControlStyle.DEFAULT }
    }
	map = new google.maps.Map(document.getElementById("map_wrapper"), mapOptions);
    blockAreaLayer = new google.maps.KmlLayer ({url: domain+'grid/grid.kml?shi=' + (new Date()).getTime()});
    bodAreaLayer = new google.maps.KmlLayer ({url: domain+'grid/border.kml?shi=' + (new Date()).getTime()});
    gapAreaLayer = new google.maps.KmlLayer ({url: domain+'grid/gap.kml?shi=' + (new Date()).getTime()});
    pc1AreaLayer = new google.maps.KmlLayer ({url: domain+'grid/A1.kml?shi=' + (new Date()).getTime()});
    pc2AreaLayer = new google.maps.KmlLayer ({url: domain+'grid/A2.kml?shi=' + (new Date()).getTime()});
    pc3AreaLayer = new google.maps.KmlLayer ({url: domain+'grid/B1.kml?shi=' + (new Date()).getTime()});
    pc4AreaLayer = new google.maps.KmlLayer ({url: domain+'grid/B2.kml?shi=' + (new Date()).getTime()});

}

var generateTiles = function(percent, currentMap, overlaypath){
	var mapMinZoom = 5;
	var mapMaxZoom = 120;
	var pix4tiler = new google.maps.ImageMapType({
		getTileUrl: function(coord, zoom) {
			var proj = currentMap.getProjection();
			var tileSize = 256 / Math.pow(2, zoom);
			//var tileBounds = new google.maps.LatLngBounds(proj.fromPointToLatLng(new google.maps.Point(coord.x * tileSize, (coord.y + 1) * tileSize)), proj.fromPointToLatLng(new google.maps.Point((coord.x + 1) * tileSize, coord.y * tileSize)));

			if ((zoom >= mapMinZoom) && (zoom <= mapMaxZoom)) {
                console.log(zoom);
				return overlaypath + zoom + "/" + coord.x + "/" +  coord.y + ".png";}
			else {
				return "http://none.png";}
		},
		tileSize: new google.maps.Size(256, 256),
		isPng: true,
		opacity: parseFloat(percent) / 100.0
	});
	return pix4tiler;
}

function loadOrthoHD(e) {
    var pix4tiler = generateTiles('100', map, domain+"ortho/");
	if(!e.checked) {
		map.overlayMapTypes.pop();
		return;
	}
	map.overlayMapTypes.insertAt(0, pix4tiler);
}

function loadCADLayer(e) {

    updateKml(e, blockAreaLayer);
    console.log(blockAreaLayer);
}
function loadBODLayer(e) {

    updateKml(e, bodAreaLayer);
    console.log(bodAreaLayer);
}
function loadGAPLayer(e) {

    updateKml(e, gapAreaLayer);
    console.log(gapAreaLayer);
}
function loadPCLayer(e) {

    updateKml(e, pc1AreaLayer);
    updateKml(e, pc2AreaLayer);
    updateKml(e, pc3AreaLayer);
    updateKml(e, pc4AreaLayer);
    console.log(pc1AreaLayer);
}

function updateKml(elem, layer) {
	if(elem.checked)
    {
        layer.setMap(map);
        console.log("UpdateKML IF");
	}
    else
    {
		layer.setMap(null);
        console.log("UpdateKML else");
	}
}

function generate_grid() {
                        document.getElementById("add").disabled = true;
                        document.getElementById('add').style.backgroundColor = '#808080';
                        document.getElementById("delete").disabled = true;
                        document.getElementById('delete').style.backgroundColor = '#808080';
                        document.getElementById("show").disabled = true;
                        document.getElementById('show').style.backgroundColor = '#808080';
                        document.getElementById("refresh").disabled = true;
                        document.getElementById('refresh').style.backgroundColor = '#808080';
						document.getElementById('generate').style.backgroundColor = "red"
						google.maps.event.addListener(map, 'click', function (event) {
						var size=storeCoordinate(event.latLng, coords);
						var property = document.getElementById('generate');
						console.log(size);
						if(size>=4)
					    {
						var triangleCoords = [coords[0], coords[1], coords[2], coords[3]];
							myPolygon = new google.maps.Polygon({
							paths: triangleCoords,
							draggable: false, // turn off if it gets annoying
							editable: false,
							strokeColor: '#FFFFFF',
							strokeOpacity: 0.8,
							strokeWeight: 2,
							fillColor: '#FFFFFF',
							fillOpacity: 0
					    });
                        myPolygon.setMap(map);
                        allPolygons.push(myPolygon);
                        console.log(myPolygon);
                         m = angular.element(document.getElementById("mymap")).scope().getJson();
                         for ( var i in data ){
                         var xx = new google.maps.LatLng(data[i]["Lat1"],data[i]["Log1"]);
                         var xy = new google.maps.LatLng(data[i]["Lat2"],data[i]["Log2"]);
                         var yx = new google.maps.LatLng(data[i]["Lat3"],data[i]["Log3"]);
                         var yy = new google.maps.LatLng(data[i]["Lat4"],data[i]["Log4"]);
                         //var desc = data[i]["description"];
                         if(google.maps.geometry.poly.containsLocation(xx,myPolygon) &&  google.maps.geometry.poly.containsLocation(yx, myPolygon) &&   google.maps.geometry.poly.containsLocation(xy, myPolygon)  &&   google.maps.geometry.poly.containsLocation(yy, myPolygon)){
                         var polygonCoords=[xx,xy,yx,yy];
                         console.log("testing");
                         grid[i]=new google.maps.Polygon({
                                    paths:polygonCoords,
                                    strokeColor: '#0000FF',
                                    strokeOpacity: 1,
                                    strokeWeight: 2,
                                    fillColor: '#0000FF',
                                    fillOpacity: 0.0
                                    });
                            grid[i].setMap(map);
                            grid[i].addListener('click', function(event) {
                            index=0;
                            console.log("contentString	"+typeof(event.latLng.lat()));
                            index=getIndex(event.latLng.lat(),event.latLng.lng())
                            infoWindow=new google.maps.InfoWindow;
                            infoWindow.setContent(data[index]["description"]);
                            infoWindow.setPosition(event.latLng);
                            infoWindow.open(map);
                        });
                    }
                }
            }
        });
	    // Create new marker on single click event on the map
		google.maps.event.addListener(map, 'click', function (event) {
		    if (markers.length < 4) {
				var marker = new google.maps.Marker({
				    position: event.latLng,
					icon: {
									path: google.maps.SymbolPath.CIRCLE,
									scale: 4,
									strokeColor: '#228B22',
									strokeOpacity: 0.8,
									strokeWeight: 2,
									fillColor: '#FFFFFF',
									fillOpacity: 0.35,
									},
									map: map,
									title: event.latLng.lat() + ', ' + event.latLng.lng()
							});
							markers.push(marker);
					}
				 });

}
function add_grid(){
                    document.getElementById("generate").disabled = true;
                    document.getElementById('generate').style.backgroundColor = '#808080';
                    document.getElementById("roi_reset").disabled = true;
                    document.getElementById('roi_reset').style.backgroundColor = '#808080';
                    document.getElementById('add').style.backgroundColor = "red"
	                google.maps.event.addListener(map, 'click', function (event) {
					var size=storeCoordinate(event.latLng, arr);
					var property = document.getElementById('add');
					console.log(size);
					if(size>=4)
					{
					newCoords = [arr[0], arr[1], arr[2], arr[3]];
					newPolygon = new google.maps.Polygon({
						paths: newCoords,
						draggable: false, // turn off if it gets annoying
						editable: false,
						strokeColor: '#FFFFFF',
						strokeOpacity: 0.8,
						strokeWeight: 2,
						fillColor: '#FFFFFF',
						fillOpacity: 0
					});
					newPolygon.setMap(map);
					document.getElementById('myModal').style.display = "block";
					}
				 });
				  google.maps.event.addListener(map, 'click', function (event) {
						 if (markers.length < 4) {
								 var marker = new google.maps.Marker({
										 position: event.latLng,
								    		 icon: {
												 path: google.maps.SymbolPath.CIRCLE,
												 scale: 4,
												 strokeColor: '#228B22',
												 strokeOpacity: 0.8,
												 strokeWeight: 2,
												 fillColor: '#FFFFFF',
												 fillOpacity: 0.35,
										 },
										 map: map,
										 title: event.latLng.lat() + ', ' + event.latLng.lng()
								 });

								 markers.push(marker);
						 }
				 });
}

function submit_grid(){
                    var g_id = document.getElementById('gid').value;
                    var gc =  document.getElementById('gc').value;
                    var cc = document.getElementById('cc').value;
                    var si =  document.getElementById('si').value;
                    var ndvi = document.getElementById('ndvi').value;
                    var gt = $("input[name='pt']:checked").val();
                    var cp =  document.getElementById('cp').value;
                    document.getElementById('myModal').style.display = "none";
                    var d={"Lat4":arr[0].lat(),"Log4":arr[0].lng(),"Lat3":arr[1].lat(),"Log3":arr[1].lng(),"Lat2":arr[2].lat(),"Log2":arr[2].lng(),"Lat1":arr[3].lat(),"Log1":arr[3].lng(),"description": "Sweet Corn<BR><BR><B>Grid_ID</B> ="+ g_id +"<BR><B>Crop_Count</B> ="+ gc + "<BR><B>Canopy_Cov</B> = "+ cc +"<BR><B>Stress_Ind</B> = "+ si +"<BR><B>NDVI</B> = "+ ndvi +"<BR><B>Grid_Type</B> = "+ gt +"<BR><B>Canopy_Per</B> = "+ cp ,"type":"manual"};
                    var result=0
                    result=angular.element(document.getElementById("mymap")).scope().putJson(d);
                        setTimeout(function() {
                        console.log('first 10 secs');
                        window.location.reload();
                        }, 1000);
}

function show_grid() {
                 document.getElementById("generate").disabled = true;
                 document.getElementById('generate').style.backgroundColor = '#808080';
                 document.getElementById("roi_reset").disabled = true;
                 document.getElementById('roi_reset').style.backgroundColor = '#808080';
                 document.getElementById("legend").style.display= "block";
                 var m=0;
                 //m=angular.element(document.getElementById("mymap")).scope();
                 m= angular.element(document.getElementById("mymap")).scope().getJson();
			 	 for ( var i in data){
                 //console.log("logggggggggggggg")
				 var xx = new google.maps.LatLng(data[i]["Lat1"],data[i]["Log1"]);
 				 var xy = new google.maps.LatLng(data[i]["Lat2"],data[i]["Log2"]);
 				 var yx = new google.maps.LatLng(data[i]["Lat3"],data[i]["Log3"]);
 				 var yy = new google.maps.LatLng(data[i]["Lat4"],data[i]["Log4"]);
		 	 	 var polygonCoords=[xx,xy,yx,yy];
				 console.log("testing");
				 if(data[i]["type"]=="automatic"){
	 			 grid[i]=new google.maps.Polygon({
	 			 paths:polygonCoords,
							strokeColor: '#FF0000',
							strokeOpacity: 1,
							strokeWeight: 2,
							fillColor: 'DFF0000',
							fillOpacity: 0.0
	 						});}
	 			else{
	 			grid[i]=new google.maps.Polygon({
	 			 paths:polygonCoords,
							strokeColor: '#0000FF',
							strokeOpacity: 1,
							strokeWeight: 2,
							fillColor: 'DFF0000',
							fillOpacity: 0.0
	 						});}

	 			grid[i].setMap(map);
				grid[i].addListener('click', function(event) {
				index=0;
				console.log("contentString	"+typeof(event.latLng.lat()));
				index=getIndex(event.latLng.lat(),event.latLng.lng())
				infoWindow=new google.maps.InfoWindow;
				infoWindow.setContent(data[index]["description"]);
				infoWindow.setPosition(event.latLng);
				infoWindow.open(map);

				    });
		 	    }
	}

function delete_grid() {
    document.getElementById("generate").disabled = true;
    document.getElementById('generate').style.backgroundColor = '#808080';
    document.getElementById("roi_reset").disabled = true;
    document.getElementById('roi_reset').style.backgroundColor = '#808080';
    document.getElementById('delete').style.backgroundColor = "red";
    for ( var i in data ){
    grid[i].addListener('click', function(event) {
	index=0;
	index=getIndex(event.latLng.lat(),event.latLng.lng())
    console.log("from fn."+index);
    angular.element(document.getElementById("mymap")).scope().deleteJson(index);
    setTimeout(function() {
    console.log('first 10 secs');
    window.location.reload();


}, 2000);

    });

		 	}}

function storeCoordinate(latLang, array) {
			array.push(latLang);
			return array.length;
		}

function ResetMap() {
		setMapOnAll(null);
		markers = [];
		coords=[];
		subMarkers=[];
		allPolygons=[];
		myPolygon = null;
		window.location.reload();}

function setMapOnAll(map) {
		var i;
		for (i = 0; i < markers.length; i++) {
				markers[i].setMap(map);
		}
		for (i = 0; i < subMarkers.length; i++) {
				subMarkers[i].setMap(map);
		}
		for (i = 0; i < allPolygons.length; i++) {
				allPolygons[i].setMap(map);
		}
}
function getIndex(x,y){
	var l = new google.maps.LatLng(x,y);
		 //var desc = data[i]["description"];
		 for(var i in grid){
		 if(google.maps.geometry.poly.containsLocation(l,grid[i])){
			 console.log("index "+i);
			 return i;
}
}
return null;
}
var init = function () {
	updateUserName();
	angular.element(document.getElementById("mymap")).scope().getJson();
	var orthoHD = document.getElementById("orthoHD");
	loadOrthoHD(orthoHD);
}


window.onload = init;
